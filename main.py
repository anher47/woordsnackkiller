with open('woordenlijst.txt', 'r') as file:
    woordenlijst = list(line.strip('\n') for line in file)


def woordSnackBot():
    resultaat = []
    usrInput = input("Type something to test this out: ")
    length = input("length: ")
    intLength = int(length)
    for word in woordenlijst:
        possibleWord = True
        inputWoord = list(usrInput)
        for letter in word:
            if letter not in inputWoord:
                possibleWord = False
                break
            else:
                inputWoord.remove(letter)
        if possibleWord:
            if len(word) == intLength:
                resultaat.append(word)
    return resultaat


print(woordSnackBot())
file.close()
